"use strict";
global.__ = {};

__.NAME = "ms_vass";
__.PORT = parseInt(process.env.PORT);
__.DEBUG = process.env.DEBUG === "TRUE" ? true : false;

// SECRETS
__.DB = {
  FIREBASEDB_URL: process.env.FIREBASEDB_URL,
};
