"use strict";
const adminFirebase = require("firebase-admin");
const serviceAccount = require("../../vass-7d414-firebase-adminsdk-7roi8-d846006817.json");
const UTIL = require.main.require("./util");
const msg = require("../msg");
let db = null;

///////////////////////////////////////////////////////////////////////////////////
///// PROMISIFY  METHODS //////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
//conexion a la base de datos firebase
let db_firebase_connect = async (args, success, fail) => {
  try {
    adminFirebase.initializeApp({
      credential: adminFirebase.credential.cert(serviceAccount),
      databaseURL: __.DB.FIREBASEDB_URL,
    });
    db = adminFirebase.firestore();
    success(db);
  } catch (error) {
    console.log(error);
    fail("database_connection_fail");
  }
};

let db_firebase_write = async (payload, success, fail) => {
  try {
    const docRef = await db
      .collection(payload.collectionName)
      .doc(payload.args.sharedkey);

    docRef.set(payload.args);
    success(payload.args);
  } catch (error) {
    fail(error);
  }
};

let db_firebase_read = async (payload, success, fail) => {
  try {
    let user = await db.collection(payload.collectionName).doc(payload.sharedkey);
    let data = await user.get();
    success(data.data());
  } catch (error) {
    fail(error);
  }
};

let db_firebase_read_all = async (payload, success, fail) => {
  try {
    db.collection(payload.collectionName)
      .get()
      .then((querySnapshot) => {
        let list = [];
        querySnapshot.forEach((doc) => {
          let data = doc.data();
          data.id = doc.id;
          list.push(data);
        });
        success(list);
      });
  } catch (error) {
    fail(error);
  }
};

///////////////////////////////////////////////////////////////////////////////////
///// MODULE EXPORTS //////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

module.exports = {
  connect: () => {
    let method = db_firebase_connect;
    return UTIL.promisify(method);
  },
  insert: (args) => {
    let method = db_firebase_write;
    return UTIL.promisify(method, args);
  },
  find: (args) => {
    let method = db_firebase_read;
    return UTIL.promisify(method, args);
  },
  findAll: (args) => {
    let method = db_firebase_read_all;
    return UTIL.promisify(method, args);
  }
};
