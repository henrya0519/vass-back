"use strict";
const assert = require("assert");
const { create } = require("domain");
const UTIL = require.main.require("./util");
const db = require("../db/firebase");
const msg = require("../msg");

///////////////////////////////////////////////////////////////////////////////////
///// PROMISIFY  METHODS //////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

let createUser = (_, success, fail) => {
  _.args.sharedkey = generateString(7,_.args.businessid)
  db.insert(_)
    .then((resp) => {
      success(resp)
    })
    .catch((error) => {
      console.error(error);
      fail(error);
    });
};


function generateString(length, characters) {
  let name ='';
  characters = characters.split(" ");
  for(let i=0; i<characters.length;i++)
  {
    name = name + characters[i]
  }
  let result = '';
  const charactersLength = name.length;
  for ( let i = 0; i < length; i++ ) {
      result += name.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}


///////////////////////////////////////////////////////////////////////////////////
///// MAIN ENDPOINT HANDLER  METHOD ///////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

// crear usuarios que se guardan en base de datos

let main_request_handler = (req, res, next) => {
  let _ = {
    args:{
      ...req.body,
    },
    collectionName:'usuarios'
    
  };
  UTIL.promisify(createUser, _)
    .then((token) => {
      UTIL.response_success(req, res, token);
    })
    .catch((error) => {
      UTIL.response_error(req, res, error);
    });
};

///////////////////////////////////////////////////////////////////////////////////
///// MODULE EXPORTS //////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

module.exports = {
  main: (req, res, next) => {
    let method = main_request_handler;
    UTIL.main_request_handler(method, req, res, next);
  },
};
