"use strict";
const assert = require("assert");
const { create } = require("domain");
const UTIL = require.main.require("./util");
const db = require("../db/firebase");
const msg = require("../msg");


///////////////////////////////////////////////////////////////////////////////////
///// PROMISIFY  METHODS //////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

//login por hive y por usuario de base de datos envio de json web token para crear seguridad en el flujo
//del backend con el frontend
let findUsers = (_, success, fail) => {
  _.collectionName ='usuarios'
  db.findAll(_)
    .then((resp) => {
      success(resp)
    })
    .catch((error) => {
      fail(msg.ERROR.MSG_002);
    });
};

///////////////////////////////////////////////////////////////////////////////////
///// MAIN ENDPOINT HANDLER  METHOD ///////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

let main_request_handler = (req, res, next) => {
  let _ = {
    ...req.body,
  };
  UTIL.promisify(findUsers, _)
    .then((resp) => {
      UTIL.response_success(req, res, resp);
    })
    .catch((error) => {
      console.log("error: ", error);
      UTIL.response_error(req, res, error);
    });
};

///////////////////////////////////////////////////////////////////////////////////
///// MODULE EXPORTS //////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

module.exports = {
  main: (req, res, next) => {
    let method = main_request_handler;
    UTIL.main_request_handler(method, req, res, next);
  },
};
