"use strict";
require("./config");

const jwtUtils = require("./security/jwtutils.js");
const express = require("express");
const app = express();
const cors = require("cors");
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
const UTIL = require("./util");
const DB_FIREBASE = require("./db/firebase");

const ENDPOINT_CREATE_USER = require("./endpoints/create_user");
const ENDPOINT_FIND_USER = require("./endpoints/find_user");
const ENDPOINT_GET_USERS = require("./endpoints/get_users");

const BASE_PATH = "/api";

app.get(BASE_PATH + "/info", jwtUtils.authenticateToken, (req, resp) => {
  resp.send("Ok");
});
app.post(BASE_PATH + "/create_user", ENDPOINT_CREATE_USER.main);
app.post(BASE_PATH + "/find_user", ENDPOINT_FIND_USER.main);
app.get(BASE_PATH + "/get_users", ENDPOINT_GET_USERS.main);


app.use(UTIL.not_found);

DB_FIREBASE.connect()
  .then((mongo) => {
    app.listen(__.PORT, () => console.log(`Listen on PORT ${__.PORT}`));
  })
  .catch((error) => {
    console.log(error);
  });
