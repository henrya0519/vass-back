class users {
  constructor(email, firstName, lastName, password, created, updated) {
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.password = password;
    this.created = created;
    this.updated = updated;
  }
}
